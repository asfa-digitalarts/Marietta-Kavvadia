var video;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(51);
  video = createCapture(VIDEO);
  video.size(windowWidth, windowHeight);
  video.hide();
  imageMode(CENTER);
}

function draw() {
  background(0);
  tint(40, 0, 250);
  image(video, width/2, height/2, mouseX, height);
  drawText();

  if (key==="1") {
    fill(255);
    textSize(30);
    text("UGLY", 1000, 100);
  }
  if (key==="2") {
    fill(255);
    textSize(30);
    text("FAT", 800, 350);
  }
  if (key==="3") {
    fill(255);
    textSize(30);
    text("MONSTROUS", 500, 600);
  }

  if (key==="4") {
    fill(255);
    textSize(30);
    text("GROTESQUE", 400, 400);
  }

  if (key==="5") {
    fill(255);
    textSize(30);
    text("OVERSIZE", 900, 900);
  }

  if (key==="6") {
    fill(255);
    textSize(30);
    text("UNDESIRABLE", 1000, 700);
  }

  if (key==="7") {
    fill(255);
    textSize(30);
    text("CHARMLESS", 800, 100);
  }
  if (key==="8") {
    fill(255);
    textSize(30);
    text("REPELLING", 900, 800);
  }

  if (key==="9") {
    fill(255);
    textSize(30);
    text("GROSS", 700, 400);
  }

  if (key==="0") {
    fill(255);
    textSize(30);
    text("SKINNY", 800, 900);
  }
}


function drawText() {
  text('body dismorphic disorder', 50, 50);
  textSize(30);
  fill(255);
}

function mousePressed() {
  let fs = fullscreen();
  fullscreen(!fs);
}
