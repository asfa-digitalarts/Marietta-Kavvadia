let video1;
let video2;
var videoX = 100;
var videoY = 100;


function preload() {
  video1 = createVideo(['data/mouth2.mp4']);
  video2 = createVideo(['data/1.mp4']);
  song = loadSound('data/422092__deathstardude__girlyvoice.mp3');
  //song2 = loadSound('data/177958__sclolex__water-dripping-in-cave.mp3');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  video1.hide();
  video2.hide();
  song.play();
  //  song2.play();
  song.setVolume(0.1);
}

function draw() {
  background(150);
  //drawText();
  imageMode(CENTER);
  image(video1, windowWidth/2, windowHeight/2, windowWidth, windowHeight);
  imageMode(CENTER);
  image(video2, videoX, videoY, video2.width, video2.height);
}

function keyPressed() {

  if (keyCode === ENTER) {
    video1.loop();
    video2.loop();
  }
}

function mouseDragged() {
  if (mouseX > videoX - 150 && mouseX < videoX + 150) {
    if (mouseY > videoY - 150 && mouseY < videoY + 150) {
      videoX = mouseX;
      videoY = mouseY;
      song.play();
    }
  }
}

//function drawText() {
//  text('but you can not eat', 0, 0);
// textSize(40);
// background(150);
//}

function mousePressed() {
  let fs = fullscreen();
  fullscreen(!fs);
}
